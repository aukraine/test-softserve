# Test SoftServe


-Back-End

```

python -m pip install --upgrade pip

pip install virtualenv
virtualenv venv
venv\Scripts\activate.bat  # for Windows
source venv/bin/activate  #for Linux and Mac 
pip install -r requirements.txt

-Enter in `test_project` folder.

python manage.py migrate
python manage.py runserver
```


-Front-End

```
-Firstly, enter in `frontend` folder.

install node.js from official site # for windows
sudo pacman -S npx # for Arch linux

npm install
npm run build

npm start
```