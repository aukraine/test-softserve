Django==2.2.3
django-cors-headers==3.0.2
djangorestframework==3.9.1
pytz==2019.1
sqlparse==0.3.0
virtualenv==16.4.3
axios==0.19.0
