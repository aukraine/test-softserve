from django.db import models


class LearnPeriods(models.Model):

    class Meta:
        db_table = 'learn_periods'
        ordering = ['id']

    period = models.IntegerField(null=False)
