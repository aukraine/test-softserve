from django.db import models


class AcademicLevels(models.Model):

    class Meta:
        db_table = 'academic_levels'
        ordering = ['id']

    name_level = models.CharField(max_length=120, null=False)


