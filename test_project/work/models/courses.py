from django.db import models
from .learn_periods import LearnPeriods


class Courses(models.Model):

    class Meta:
        db_table = 'courses'
        ordering = ['id']

    name = models.CharField(max_length=120, null=False)
    created_date = models.DateTimeField(auto_now_add=True)
    f_learn_period = models.ForeignKey(LearnPeriods, on_delete=models.SET_NULL, null=True,
                                       related_name='f_learn_period_id')
