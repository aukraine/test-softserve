from django.db import models
from .courses import Courses
from .students import Students
from .teachers import Teachers


class GraduateWork(models.Model):

    class Meta:
        db_table = 'graduate_work'
        ordering = ['id']

    name_work = models.CharField(max_length=220, null=False)
    f_student = models.ForeignKey(Students, null=True, on_delete=models.SET_NULL, related_name='f_student_id')
    f_course = models.ForeignKey(Courses, null=True, on_delete=models.SET_NULL, related_name='f_course_id')
    f_teacher = models.ForeignKey(Teachers, null=True, on_delete=models.SET_NULL, related_name='f_teacher_id')
    grade = models.IntegerField(null=False)
    description = models.TextField(max_length=400, null=False)

    def __str__(self):
        return 'Craduate work: name - {name}, student - {student}, course - {course}, teacher - {teacher},' \
               ' grade - {grade}, description - {description}'.format(
                    name=self.name_work.__str__(),
                    student=self.f_student.__str__(),
                    teacher=self.f_teacher.__str__(),
                    grade=self.grade.__str__(),
                    description=self.description.__str__()
                )
