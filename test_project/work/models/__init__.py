from .graduate_work import GraduateWork
from .students import Students
from .teachers import Teachers
from .courses import Courses
from .education_forms import EducationForms
from .learn_periods import LearnPeriods
from .academic_levels import AcademicLevels
