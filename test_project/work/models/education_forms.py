from django.db import models


class EducationForms(models.Model):

    class Meta:
        db_table = 'education_forms'
        ordering = ['id']

    name_form = models.CharField(max_length=120, null=False)
