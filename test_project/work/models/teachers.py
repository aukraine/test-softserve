from django.db import models
from .academic_levels import AcademicLevels


class Teachers(models.Model):

    class Meta:
        db_table = 'teachers'
        ordering = ['id']

    first_name = models.CharField(max_length=60, null=False)
    last_name = models.CharField(max_length=60, null=False)
    f_academic_level = models.ForeignKey(AcademicLevels, on_delete=models.SET_NULL,
                                         null=True,
                                         related_name='f_academic_level_id')
