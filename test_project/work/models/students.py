from django.db import models
from .education_forms import EducationForms


class Students(models.Model):

    class Meta:
        db_table = 'students'
        ordering = ['id']

    first_name = models.CharField(max_length=60, null=False)
    last_name = models.CharField(max_length=60, null=False)
    age = models.IntegerField(null=True)
    f_education_form = models.ForeignKey(EducationForms, on_delete=models.SET_NULL, null=True,
                                         related_name='f_education_form_id')
