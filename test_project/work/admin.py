from django.contrib import admin
from .models import Students, EducationForms
from .models import AcademicLevels
from .models import Courses, LearnPeriods


# Register your models here.
admin.site.register(Students)
admin.site.register(EducationForms)
admin.site.register(AcademicLevels)
admin.site.register(Courses)
admin.site.register(LearnPeriods)
