from rest_framework import viewsets
from work.models import EducationForms
from work.serializers import EducationFormsSerializer


class EducationFormsAPIView(viewsets.ModelViewSet):
    queryset = EducationForms.objects.all()
    serializer_class = EducationFormsSerializer
