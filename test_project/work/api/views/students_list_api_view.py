from rest_framework import viewsets

from work.serializers import StudentsSerializer, EducationFormsSerializer
from work.models import Students, EducationForms


class StudentsAPIView(viewsets.ModelViewSet):
    queryset = Students.objects.all()
    serializer_class = StudentsSerializer

    def perform_create(self, serializer):
        try:
            form = EducationForms.objects.get(
                name_form=self.request.data['f_education_form'].pop(
                    'name_form'))
            serializer.save(f_education_form=form)
        except:
            'form does not exist'

    def perform_update(self, serializer):
        try:
            education_form = EducationForms.objects.get(
                name_form=self.request.data['f_education_form'].pop('name_form')
            )
            serializer.save(f_education_form=education_form)
        except:
            'form does not exist for updating'
