from rest_framework import viewsets
from ...models import Teachers, AcademicLevels
from ...serializers import TeacherSerializer


class TeacherViewsSet(viewsets.ModelViewSet):
    queryset = Teachers.objects.all()
    serializer_class = TeacherSerializer

    def perform_create(self, serializer):
        try:
            level = AcademicLevels.objects.get(
                name_level=self.request.data['f_academic_level'].pop(
                    'name_level'))
            serializer.save(f_academic_level=level)
        except:
            'level does not exist'

    def perform_update(self, serializer):
        try:
            level = AcademicLevels.objects.get(
                name_level=self.request.data['f_academic_level'].pop(
                    'name_level'))
            serializer.save(f_academic_level=level)
        except:
            'level does not exist'
