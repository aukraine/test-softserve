from rest_framework import viewsets
from ...models import AcademicLevels
from ...serializers import AcademicLevelSerializer


class AcademicLevelsViewsSet(viewsets.ModelViewSet):
    queryset = AcademicLevels.objects.all()
    serializer_class = AcademicLevelSerializer
