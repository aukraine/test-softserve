from rest_framework import viewsets
from ...models import LearnPeriods
from ...serializers import LearnPeriodsSerializer


class LearnPeriodsViewSet(viewsets.ModelViewSet):

    queryset = LearnPeriods.objects.all()
    serializer_class = LearnPeriodsSerializer
