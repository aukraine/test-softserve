from rest_framework import routers
from .views import TeacherViewsSet
from .views import AcademicLevelsViewsSet
from .views import StudentsAPIView, EducationFormsAPIView
from .views import CoursesViewSet, LearnPeriodsViewSet


router = routers.SimpleRouter()
router.register(r'^teachers', TeacherViewsSet)
router.register(r'^academic', AcademicLevelsViewsSet)
router.register(r'^education', EducationFormsAPIView)
router.register(r'^students', StudentsAPIView)
router.register(r'^courses', CoursesViewSet)
router.register(r'^periods', LearnPeriodsViewSet)

urlpatterns = []

urlpatterns += router.urls