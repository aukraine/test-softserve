# Generated by Django 2.2.3 on 2019-07-04 09:08

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Courses',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=120)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'db_table': 'courses',
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='Students',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=60)),
                ('last_name', models.CharField(max_length=60)),
                ('birthday_date', models.DateTimeField()),
            ],
            options={
                'db_table': 'students',
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='Teachers',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=60)),
                ('last_name', models.CharField(max_length=60)),
            ],
            options={
                'db_table': 'teachers',
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='GraduateWork',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name_work', models.CharField(max_length=220)),
                ('grade', models.IntegerField()),
                ('description', models.TextField(max_length=400)),
                ('f_course', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='f_course_id', to='work.Courses')),
                ('f_student', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='f_student_id', to='work.Students')),
                ('f_teacher', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='f_teacher_id', to='work.Teachers')),
            ],
            options={
                'db_table': 'graduate_work',
                'ordering': ['id'],
            },
        ),
    ]
