from rest_framework import serializers
from . import AcademicLevelSerializer
from ..models import Teachers
from ..models import AcademicLevels


class TeacherSerializer(serializers.ModelSerializer):
    f_academic_level = AcademicLevelSerializer(read_only=True)

    class Meta:
        model = Teachers
        fields = ('id', 'first_name', 'last_name', 'f_academic_level')

    # def create(self, validated_data):
    #     level = validated_data.pop('f_academic_level').pop('name_level')
    #     try:
    #         academic_level = AcademicLevels.objects.get(name=level)
    #     except:
    #         'level does not exist'
    #     teacher = Teachers.objects.create(**validated_data,
    #                                       f_academic_level=academic_level)
    #     return teacher