from rest_framework import serializers
from work.models import GraduateWork, Courses
from .courses_serializer import CoursesSerializer


class GraduateWorkSerializer(serializers.ModelSerializer):

    # student = StudentSerializer(source='f_student')
    # teacher = TeacherSerializer(source='f_teacher')
    course = CoursesSerializer(source='f_course')

    class Meta:
        model = GraduateWork
        fields = ('id', 'name_work', 'f_student', 'f_teacher',
                  'f_course', 'grade', 'description')

    def create(self, validated_data):
        validated_data['f_course'] = dict(validated_data['f_course'])
        course = Courses.objects.get(name=validated_data.pop('f_course').get('id'))
        instance = Courses.objects.create(name_work=validated_data.get('name_work'),
                                          grade=validated_data.get('grade'),
                                          description=validated_data.get('description'),
                                          f_course=course)
        return instance
