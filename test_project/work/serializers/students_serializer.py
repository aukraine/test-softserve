from rest_framework import serializers
from work.models import Students, EducationForms
from .education_forms_serializer import EducationFormsSerializer


class StudentsSerializer(serializers.ModelSerializer):
    f_education_form = EducationFormsSerializer(read_only=True)

    class Meta:
        model = Students
        fields = ('id', 'first_name', 'last_name', 'age', 'f_education_form')
