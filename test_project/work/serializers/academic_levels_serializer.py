from rest_framework import serializers
from ..models import AcademicLevels


class AcademicLevelSerializer(serializers.ModelSerializer):
    # f_academic_level_id = serializers.StringRelatedField()
    class Meta:
        model = AcademicLevels
        fields = ('name_level',)



