from rest_framework import serializers
from work.models import LearnPeriods


class LearnPeriodsSerializer(serializers.ModelSerializer):

    class Meta:
        model = LearnPeriods
        fields = ('id', 'period',)
