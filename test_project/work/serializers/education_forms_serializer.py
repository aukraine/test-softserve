from rest_framework import serializers
from work.models import EducationForms


class EducationFormsSerializer(serializers.ModelSerializer):

    class Meta:
        model = EducationForms
        fields = ('id', 'name_form')
