from rest_framework import serializers
from ..models import Courses, LearnPeriods
from .learn_periods_serializer import LearnPeriodsSerializer


class CoursesSerializer(serializers.ModelSerializer):

    period = LearnPeriodsSerializer(source='f_learn_period')

    class Meta:
        model = Courses
        fields = ('id', 'name', 'created_date', 'period')

    def create(self, validated_data):

        period = LearnPeriods.objects.get(period=validated_data.pop('f_learn_period').get('period'))
        instance = Courses.objects.create(name=validated_data.get('name'),
                                          created_date=validated_data.get('created_date'),
                                          f_learn_period=period)
        return instance

    def update(self, instance, validated_data):

        period = LearnPeriods.objects.get(period=validated_data.pop('f_learn_period').get('period'))
        instance.name = validated_data.get('name', instance.name)
        instance.f_learn_period = validated_data.get('f_learn_period', period)
        instance.save()
        return instance

