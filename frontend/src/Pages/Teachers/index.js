import React from "react";
import "./teachers.scss";
import ButtonAppBar from "../../Components/MainNavigation";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import Paper from "@material-ui/core/Paper";
import axios from "axios";
import AddIcon from "@material-ui/icons/Add";
import Fab from "@material-ui/core/Fab";
import Tooltip from "@material-ui/core/Tooltip";


export class TeachersPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      teachers: [],
      snackbar: false,
      pushURL: "teachers/"
    };
  }

  componentDidMount() {
    axios.get(this.state.pushURL).then(response => {
      this.setState({ teachers: response.data });
    });
  }

  handleClose() {
    this.setState({ snackbar: false });
  }

  handleAdd() {
    this.props.history.push("/teachers/new/");
  }

  edit(index) {
    this.props.history.push("/teachers/" + index);
  }
  delete(index) {
    axios.delete(this.state.pushURL + index).then(response => {
      const teachers = this.state.teachers.filter(m => m.id !== index);
      this.setState({
        teachers: teachers,
        message: "Teacher was deleted",
        snackbar: true
      });
    });
  }

  render() {
    return (
      <>
        <ButtonAppBar />
        <Paper className="paperStyle" elevation={20}>
          <Table height={"auto"}>
            <TableHead>
              <TableRow>
                <TableCell tooltip="FirstName" align="center">
                  First Name
                </TableCell>
                <TableCell tooltip="LastName" align="center">
                  Last Name
                </TableCell>
                <TableCell tooltip="AcademicLevel" align="center">
                  Academic level
                </TableCell>
                <TableCell tooltip="Actions" align="center">
                  Actions
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.teachers.map((row, index) => (
                <TableRow key={index}>
                  <TableCell align="center">{row.first_name}</TableCell>
                  <TableCell align="center">{row.last_name}</TableCell>
                  <TableCell align="center">
                    {row.f_academic_level["name_level"]}
                  </TableCell>

                  <TableCell align="center">
                    <IconButton
                      aria-label="Edit"
                      onClick={() => this.edit(row.id)}
                    >
                      <EditIcon />
                    </IconButton>
                    <IconButton
                      aria-label="Delete"
                      onClick={() => this.delete(row.id)}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))}
              <TableRow>
                <TableCell align="center">
                  <Tooltip
                    title="Add"
                    aria-label="Add"
                    onClick={this.handleAdd.bind(this)}
                  >
                    <Fab color="primary">
                      <AddIcon />
                    </Fab>
                  </Tooltip>
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>

          <Snackbar
            open={this.state.snackbar}
            message={this.state.message}
            autoHideDuration={4000}
            onClose={this.handleClose.bind(this)}
          />
        </Paper>
      </>
    );
  }
}
