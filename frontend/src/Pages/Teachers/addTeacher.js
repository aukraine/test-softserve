import React from "react";
import "./teachers.scss";
import ButtonAppBar from "../../Components/MainNavigation";
import Paper from "@material-ui/core/Paper";
import axios from "axios";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { AcademicLevel } from "../../Components/TeacherAdd/dropDown";
axios.defaults.baseURL = "http://localhost:8000/api/";

export class AddTeachersPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      lastName: {
        value: ""
      },
      firstName: {
        value: ""
      },
      academicLevel: {
        value: ""
      },
      academicLevels: [],
      snackbar: false,
      pushURL: "academic/",
      teacherURL: "teachers/"
    };
  }

  componentDidMount() {
    if (this.props.match.params.id === "new") {
      axios.get(this.state.pushURL).then(response => {
        this.setState({ academicLevels: response.data });
      });
    } else {
      axios.get(this.state.pushURL).then(response => {
        this.setState({ academicLevels: response.data });
      });
      axios
        .get(this.state.teacherURL + this.props.match.params.id + "/")
        .then(response => {
          this.setState({
            lastName: { value: response.data["last_name"] },
            firstName: { value: response.data["first_name"] },
            academicLevel: {
              value: response.data.f_academic_level["name_level"]
            }
          });
        });
    }
  }
  setEnabledButton() {
    if (
      this.state.lastName["value"] !== "" &&
      this.state.firstName["value"] !== "" &&
      this.state.academicLevel["value"] !== ""
    ) {
      return false;
    } else {
      return true;
    }
  }
  handleChange(evt) {
    this.setState({ [evt.target.name]: { value: evt.target.value } });
  }
  handleSubmit() {
    const intData = {
      first_name: this.state.firstName.value,
      last_name: this.state.lastName.value,
      f_academic_level: {
        name_level: this.state.academicLevel.value
      }
    };
    if (this.props.match.params.id === "new") {
      axios
        .post(this.state.teacherURL, intData)
        .then(res => {
          this.props.history.push("/teachers/");
        })
        .catch(error => {
          console.log(error);
        });
    } else {
      axios
        .put(this.state.teacherURL + this.props.match.params.id + "/", intData)
        .then(res => {
          this.props.history.push("/teachers/");
        })
        .catch(error => {
          console.log(error);
        });
    }
  }

  render() {
    return (
      <>
        <ButtonAppBar />
        <Paper className="paperStyle" elevation={20}>
          <form>
            <h3>First Name</h3>
            <TextField
              id="firstName"
              value={this.state.firstName["value"]}
              helperText="Input first name"
              name="firstName"
              onChange={this.handleChange.bind(this)}
              className="textFieldStyle"
            />
            <h3>Last Name</h3>
            <TextField
              id="lastName"
              value={this.state.lastName.value}
              helperText="Input first name"
              name="lastName"
              onChange={this.handleChange.bind(this)}
              className="textFieldStyle"
            />
            <h3>Academic Level</h3>
            <AcademicLevel
              values={this.state.academicLevels}
              handleChange={this.handleChange.bind(this)}
              selectValue={this.state.academicLevel.value}
              name={"academicLevel"}
              id="academicLevel"
            />
            <br />
            <br />
            <Button
              id="Button"
              variant="contained"
              color="primary"
              onClick={this.handleSubmit.bind(this)}
              className="buttonStyle"
              disabled={this.setEnabledButton()}
            >
              Save
            </Button>
            <br />
            <br />
          </form>
        </Paper>
      </>
    );
  }
}
