import React from "react";
import "./home.scss";
import ButtonAppBar from "../../Components/MainNavigation";
import { NavLink } from "react-router-dom";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import Paper from "@material-ui/core/Paper";

export class MainPage extends React.Component {
  render() {
    return (
      <>
        <ButtonAppBar />
        <Paper className="paperStyle" elevation={20}>
          <Table height={"auto"}>
            <TableHead>
              <TableRow>
                <TableCell tooltip="1">1</TableCell>
                <TableCell tooltip="Actions">Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow>
                <TableCell>1</TableCell>
                <TableCell>
                  <NavLink to={""}>
                    <IconButton aria-label="Edit">
                      <EditIcon />
                    </IconButton>
                  </NavLink>
                  <IconButton aria-label="Delete">
                    <DeleteIcon />
                  </IconButton>
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
          <Snackbar />
        </Paper>
      </>
    );
  }
}
