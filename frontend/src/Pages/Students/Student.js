import React from 'react';
import {withRouter} from 'react-router-dom';
// import {style} from 'src/layout';
import axios from 'axios';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import Paper from '@material-ui/core/Paper';
import Select from '@material-ui/core/Select';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import {EducationForm} from "../../Components/DropDownEducationForm";
// import { styles } from './../../styles/styles';
// import { withStyles } from '@material-ui/core/styles';


const getURL = '/education/';
const postURL = '/students/';

/**
 * Creates a form to create Stage
 */
export class Student extends React.Component {
  /**
   * Constructor of class
   * @param {props} props - Properties of object
   */
  constructor(props) {
    super(props);

    this.state = {
      first_name: {
        value: '',
        error: false,
        label: ' ',
      },
      last_name: {
        value: '',
        error: false,
        label: ' ',
      },
      age: {
        value: '',
        error: false,
        label: ' ',
      },
      education_form: {
        id:'',
        value: '',
        error: false,
        label: ' ',
      },
      field: {
        value: '',
        error: false,
        label: ' ',
      },
      educationField: [],
      // student_field: [],
      snackbar: {
        open: false,
        message: '',
      },
      submitButton: {
        disabled: true,
      },
    };

    this.style = {
      fontFamily: 'Roboto',
      height: 'auto',
      width: '60%',
      margin: 'auto',
      marginLeft: '20%',
      minWidth: 300,
      position: 'relative',
      textAlign: 'center',
      display: 'inline-block',

      nameDiv: {
        display: 'block',
        margin: 'auto',
        paddingBottom: '2em',
      },
      formControl: {
        minWidth: '35%',
      },
      textField: {
        minWidth: '35%'
      },
    };
  }

  /**
   * Change handler for FirstName
   * @param {event} event - Send event from object
   */
  handleChangeFirstName(event) {
    if (event.target.value === '') {
      this.setState({
        field: {
          error: true,
          label: 'First Name can\'t be empty',
        },
        first_name:{
          value: '',
        },
      });
      this.setState({submitButton: {disabled: true}});
    } else {
      this.setState({
        first_name: {
          value: event.target.value
        },
        field: {
          error: false,
          label: ' ',
        },
      });
      this.setState({submitButton: {disabled: false}});
    }
  }

  /**
   * Change handler for LastName
   * @param {event} event - Send event from object
   */
  handleChangeLastName(event) {
    if (event.target.value === '') {
      this.setState({
        field: {
          error: true,
          label: 'Last Name can\'t be empty',
        },
        last_name:{
          value: '',
        },
      });
      this.setState({submitButton: {disabled: true}});
    } else {
      this.setState({
        last_name: {
          value: event.target.value
        },
        field: {
          error: false,
          label: ' ',
        },
      });
      this.setState({submitButton: {disabled: false}});
    }
  }

  /**
   * Change handler for Age
   * @param {event} event - Send event from object
   */
  handleChangeAge(event) {
    if (event.target.value === '') {
      this.setState({
        field: {
          error: true,
          label: 'Age can\'t be empty',
        },
        age:{
          value: '',
        },
      });
      this.setState({submitButton: {disabled: true}});
    } else {
      this.setState({
        age: {
          value: parseInt(event.target.value)
        },
        field: {
          error: false,
          label: ' ',
        },
      });
      this.setState({submitButton: {disabled: false}});
    }
  }

  /**
   * Change handler for Education
   * @param {event} event - Send event from object
   * @param {value} value - Send event from object
   */
  handleChangeEducation(event, value) {
    this.setState({
      education_form: {
        value: value.props.children
      }
    });
  }

  /**
   * Submit handler to send data to API and show message
   * @param {event} event - Send event from object
   */
  handleSubmit(event) {
    event.preventDefault();
    const serverport = {
      first_name: this.state.first_name.value,
      last_name: this.state.last_name.value,
      age: this.state.age.value,
      f_education_form: {
        name_form: this.state.education_form.value
      },
    };
    if(this.props.match.params.id) {
      axios.put(postURL + this.props.match.params.id + '/', serverport)
        .then((res) => {
          this.setState({
            snackbar: {
              message: 'Student was updated',
              open: true,
            },
          }, () => this.props.history.push('/students'));
        })
        .catch((error) => {
          this.setState({
            snackbar: {
              message: 'Student isn\'t updated',
              open: true,
            },
          });
          console.log(error);
        });
    }else{
      axios.post(postURL, serverport)
        .then((res) => {
          this.setState({
            snackbar: {
              message: 'Student was created',
              open: true,
            },
          }, () => this.props.history.push('/students'));
        })
        .catch((error) => {
          this.setState({
            snackbar: {
              message: 'Student isn\'t created',
              open: true,
            },
          });
          console.log(error);
        });
    }
  }
  componentWillMount() {
    if(this.props.match.params.id){
      axios.get(postURL + this.props.match.params.id + '/')
        .then(function (response) {
          return response.data;
        }).then((json) => {
          this.setState({
            first_name: {value: json.first_name},
            last_name: {value: json.last_name},
            age: {value: json.age},
            education_form:{
              value: json.f_education_form.name_form
            },
            pushMethod: 'PUT',
            pushURL: '/students/' + this.props.match.params.id,
          });
            console.log(json.data);
        })
    }
    axios.get(getURL)
        .then((response) =>{
          this.setState({educationField: response.data});
        })
        .catch(function(error) {
          console.log(error);
        });
  }

  /**
   * Close Snackbar message
   */
  handleClose() {
    this.setState({snackbar: false });
  }
  setEnabledButton() {
    if (
      this.state.first_name["value"] !== "" &&
      this.state.last_name["value"] !== "" &&
      this.state.age["value"] !== "" &&
      this.state.education_form["value"] !== ""
    ) {
      return false;
    } else {
      return true;
    }
  }
  /**
   * Rendering a page
   * @return {*} - return student component form
   */
  render() {
    return (
      <React.Fragment>
        <Paper id='Paper' style={this.style} elevation={20}>
          <form
            onSubmit={this.handleSubmit.bind(this)}
          >
            <h1>Student</h1>
              <TextField
                id='first_name'
                helperText="First Name"
                onChange={this.handleChangeFirstName.bind(this)}
                className={this.style.textField}
                value={this.state.first_name.value}
                error={this.state.field.error}
                label={this.state.field.label}/>
                <br/><br/>
              <TextField
                id='last_name'
                helperText="Last Name"
                onChange={this.handleChangeLastName.bind(this)}
                className={this.style.textField}
                value={this.state.last_name.value}
                error={this.state.field.error}
                label={this.state.field.label}/>
                <br/><br/>
              <TextField
                id='age'
                helperText="Age"
                onChange={this.handleChangeAge.bind(this)}
                className={this.style.textField}
                value={this.state.age.value}
                error={this.state.field.error}
                label={this.state.field.label}/>
                <br/><br/>
                <EducationForm
                  values={this.state.educationField}
                  handleChange={this.handleChangeEducation.bind(this)}
                  selectValue={this.state.education_form.value}
                  name={this.state.education_form.value}
                  id={this.state.education_form.id}
                />
                <br/><br/>
            <Button id='Button'
              variant='contained'
              color='primary'
              type='submit'
              disabled={this.setEnabledButton()} >
              Save
            </Button>
            <br/><br/>
          </form>
          <Snackbar
            open={this.state.snackbar.open}
            message={this.state.snackbar.message}
            autoHideDuration={4000}
            onClose={this.handleClose.bind(this)}
          />
        </Paper>
      </React.Fragment>
    );
  }
}

export default (withRouter(Student));
