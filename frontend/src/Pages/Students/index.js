import React from "react";
import "./students.scss";
import ButtonAppBar from "../../Components/MainNavigation";
import { NavLink } from "react-router-dom";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import Paper from "@material-ui/core/Paper";
import axios from "axios";
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';


export class StudentsPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      students: [],
      snackbar: false,
      pushURL: "students/"
    };
  }

  componentDidMount() {
    axios.get(this.state.pushURL).then(response => {
      console.log(response.data);
      this.setState({ students: response.data });
    });
  }

  handleClose() {
    this.setState({ snackbar: false });
  }

  delete(index) {
    axios.delete(this.state.pushURL + index).then(response => {
      const students = this.state.students.filter(m => m.id !== index);
      this.setState({
        students: students,
        message: "Students was deleted",
        snackbar: true
      });
    });
  }

  render() {
    return (
      <>
      <ButtonAppBar />
        <Paper className="paperStyle" elevation={20}>
          <Table height={"auto"}>
            <TableHead>
              <TableRow>
                <TableCell tooltip="FirstName" align="center">
                  First Name
                </TableCell>
                <TableCell tooltip="LastName" align="center">
                  Last Name
                </TableCell>
                <TableCell tooltip="Age" align="center">
                  Age
                </TableCell>
                <TableCell tooltip="Education form" align="center">
                  Education Form
                </TableCell>
                <TableCell tooltip="Actions" align="center">
                  Actions
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.students.map((row, index) => (
                <TableRow key={index}>
                  <TableCell align="center">{row.first_name}</TableCell>
                  <TableCell align="center">{row.last_name}</TableCell>
                  <TableCell align="center">{row.age}</TableCell>
                  <TableCell align="center">
                    {row.f_education_form["name_form"]}
                  </TableCell>
                  <TableCell align="center">
                    <NavLink to={"/students/" + row.id + '/'}>
                      <IconButton aria-label="Edit">
                        <EditIcon />
                      </IconButton>
                    </NavLink>
                    <IconButton
                      aria-label="Delete"
                      onClick={() => this.delete(row.id)}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
          <Snackbar
            open={this.state.snackbar}
            message={this.state.message}
            autoHideDuration={4000}
            onClose={this.handleClose.bind(this)}
          />
        </Paper>
        <NavLink to={'/students/new'}>
          <Fab color="primary" >
            <AddIcon />
          </Fab>
        </NavLink>
      </>
    );
  }
}
