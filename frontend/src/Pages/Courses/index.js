import React from 'react';
import CoursesTable from '../../Components/Courses/CoursesTable';
import CoursesModal from '../../Components/Courses/CoursesModal'
import ButtonAppBar from "../../Components/MainNavigation";
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Paper from "@material-ui/core/Paper";

/**
 * Component that illustrates all courses and
 * allows CRUD ops with courses
 * @return {*} - Return bundled object
 */
export default class CoursesList extends React.Component  {

  /**
  * Constructor of class
  * @param {*} props - props from parent object
  */
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
    };
  }
  
  /**
   * set open Dialog
   */
  handleOpenModal(event) {
    this.setState({ modal: true });
  }

  /**
   * set close Dialog
   */
  handleCloseModal(event) {
    this.setState({ modal: false });
  }

  /**
   * set close Dialog and reload page
   */
  handleSubmitModal(event) {
    this.setState({ modal: false });
    window.location.reload();
  }

  /**
   * Render filled table
   * @return {*} - return component
   */
  render() {
    return (
      <React.Fragment>
        <ButtonAppBar />
        <Paper className="paperStyle" elevation={20}>
          <CoursesTable />
            <Fab color="primary" onClick={this.handleOpenModal.bind(this)} >
              <AddIcon />
            </Fab>
          <CoursesModal
            open={this.state.modal}
            onClose={this.handleCloseModal.bind(this)}
            onSubmit={this.handleSubmitModal.bind(this)}
            action='CREATE'
            method='POST'
          />
        </Paper>
      </React.Fragment>
    );
  }
}
