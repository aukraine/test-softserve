import React from "react";
import "./App.css";
import Router from "./Helpers/Router";
import axios from "axios";


axios.defaults.baseURL = "http://localhost:8000/api/";

function App() {
  return <Router />;
}

export default App;
