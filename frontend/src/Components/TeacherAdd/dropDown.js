import React from "react";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import "./style.scss";

export class AcademicLevel extends React.Component {
  constructor(props) {
    super(props);

    this.handleOpen = this.handleOpen.bind(this);
  }

  handleOpen() {
    this.setState({ open: true });
  }

  render() {
    const { values } = this.props;

    return (
      <React.Fragment>
        <form autoComplete="off">
          <FormControl className="dropStyle">
            <Select
              value={this.props.selectValue}
              onChange={this.props.handleChange}
              // displayEmpty
              name={this.props.name}
            >
              {values.map((item, index) => (
                <MenuItem value={item.name_level} key={index}>
                  {item.name_level}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </form>
      </React.Fragment>
    );
  }
}
