import React from "react";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

export class EducationForm extends React.Component {
  constructor(props) {
    super(props);

    this.handleOpen = this.handleOpen.bind(this);
  }

  handleOpen() {
    this.setState({ open: true });
  }

  tab() {
    const { values } = this.props;
    return values.map((item, index) => {
      return <MenuItem key={item.id} value={item.name_form}>{item.name_form}</MenuItem>;
    });
  }

  render() {
    return (
      <React.Fragment>
        <form autoComplete="off">
          <FormControl>
            <Select
              value={this.props.selectValue}
              onChange={this.props.handleChange}
              name={this.props.name}
            >
              {this.tab()}
            </Select>
          </FormControl>
        </form>
      </React.Fragment>
    );
  }
}
