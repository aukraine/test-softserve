import React from 'react';
import axios from 'axios';
import {withRouter} from 'react-router-dom';
import CoursesModal from './CoursesModal'
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Snackbar from '@material-ui/core/Snackbar';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

/**
 * Table for show up all courses
 */
export class CoursesTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      url: '/courses/',
      id: '',
      data: '',
      courses: [],
      snackbar: false,
      message: '',
      modal: false,
    };
  }

  /**
   * Fetch courses when component was accessed
   */
  componentWillMount() {
    axios(this.state.url)
      .then((response) => {
        this.setState({ courses: response.data });
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  /**
   * Send DELETE request for specified courses.
   * Show message when it deleted and remove it from list
   */
  deleteCourses(index) {
    axios.delete(this.state.url + index).then((response) => {
      const courses = this.state.courses.filter((course) => course.id !== index);
      this.setState({
        courses: courses,
        message: 'Courses was deleted',
        snackbar: true,
      });
    });
  }

  /**
   * Close Snackbar message
   */
  handleClose() {
    this.setState({snackbar: false });
  }

  /**
   * set open Dialog
   */
  async handleOpenModal(index) {
    axios(this.state.url + index + '/')
      .then((response) => {
        this.setState({ 
          modal: true, 
          id: index,
          data: {
            name: response.data.name,
            period: response.data.period.period,
          }
        });
      });   
  }

  /**
   * set close Dialog
   */
  handleCloseModal(event) {
    this.setState({ modal: false });
  }

  /**
   * set close Dialog and reload page
   */
  handleSubmitModal(event) {
    this.setState({ modal: false });
    window.location.reload();
  }

  /**
   * Render filled table
   * @return {*} - return component
   */
  render() {
    return (
      <Paper>
        <Table height={'auto'} width={'120%'}>
          <TableHead>
            <TableRow>
              <TableCell tooltip="The Name" align="center">Name</TableCell>
              <TableCell tooltip="The Created date" align="center">Created date</TableCell>
              <TableCell tooltip="The Learn Period" align="center">Learn Period</TableCell>
              <TableCell tooltip="The Actions" align="center">Actions</TableCell>
            </TableRow>
          </TableHead >
          <TableBody>
            {this.state.courses.map((row, index) => (
              <TableRow key={index}>
                <TableCell >{row.name}</TableCell >
                <TableCell >{row.created_date}</TableCell >
                <TableCell >{row.period.period}</TableCell >
                <TableCell >
                  <IconButton aria-label="Edit"
                    onClick={() => this.handleOpenModal(row.id)}>
                    <EditIcon aria-label="Edit">edit_icon</EditIcon>
                  </IconButton>
                  <IconButton aria-label="Delete"
                    onClick={() => this.deleteCourses(row.id)}>
                    <DeleteIcon />
                  </IconButton>
                </TableCell >
              </TableRow>
            ))}
          </TableBody>
        </Table>
        <Snackbar
          open={this.state.snackbar}
          message={this.state.message}
          autoHideDuration={2000}
          onClose={this.handleClose.bind(this)}
        />
        <CoursesModal
          open={this.state.modal}
          onClose={this.handleCloseModal.bind(this)}
          onSubmit={this.handleSubmitModal.bind(this)}
          action='UPDATE'
          method='PUT'
          id={this.state.id}
          data={this.state.data}
        />
      </Paper>
    );
  }
}

export default withRouter(CoursesTable);
