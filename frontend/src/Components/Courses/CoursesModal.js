import React from 'react';
import axios from 'axios';
import {withRouter} from 'react-router-dom';
import Select from '@material-ui/core/Select';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';


/**
 * Creates a form to create new course
 */
export class CoursesModal extends React.Component {
    
  /**
  * Constructor of class
  * @param {*} props - props from parent object
  */
  constructor(props) {
    super(props);
  
    this.state = {
      url: '/courses/',
      action: undefined,
      method: undefined,
      id: this.props.id,
      flag: null,
      flagID: null,
      periods: [],

      name: { 
        error: false, 
        value: '', 
        label: ' ' 
      },
      period: {
        id: '',
        value: '',
        error: false,
        label: ' ',
      }
    };
  }
  
  /**
   * Change handler for inputLogin
   * @param {*} event - current event
   */
  handleChangeName(event) {
    if (event.target.value === '') {
      this.setState({
        name: {
          error: true,
          label: 'Name can\'t be empty',
        }
      });
    } else {
      this.setState({
        name: {
          value: event.target.value,
          error: false,
          label: ' ',
        }
      });
    }
  };

  /**
   * Change handler for field
   * @param {event} event - Send event from object
   * @param {value} value - Send event from object
   */
  handleChangePeriod(event, value) {
    this.setState({
      period: { id: value.props.value }
    });
  }
  
  /**
   * Submit handler to send data to API and show message
   * @param {*} event - current event
   */
  handleSubmit(event) {
    const data = {
      name: this.state.name.value,
      period: {
        period: this.state.period.id,
      }
    };
    console.log(data);
    axios({
      method: this.state.method,
      url: this.state.url,
      data: data,
    })
      .then(() => this.props.onSubmit() );
  }

  /**
   * Data loader before updating a page
   */
  componentDidUpdate() {
    if (this.state.flagID === null) {
      this.setState({
        action: this.props.action,
        method: this.props.method,
        flagID: 0,
      });
    }
    if (this.props.id !== undefined && this.props.id !== '' && this.state.flag === null) {
      console.log(this.props.data);
      this.setState({
        url: '/courses/' + this.props.id + '/',
        flag: 0,
        name: { value: this.props.data.name },
        period: { id: this.props.data.period },
        action: this.props.action,
        method: this.props.method,
      })
    }
  }

  /**
   * Data loader before rendering a page
   */
  componentWillMount() {
    axios('/periods/')
      .then((response) =>{
        this.setState({periods: response.data});
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  /**
   * Data write for Ithems
   */
  handleCut() {
    return this.state.periods.map(function(object, index) {
      return <MenuItem key={index} value={object.period}>{object.period}</MenuItem>
    });
  }

  /**
   * Validation our form and set enabled button
   * @return {*} bool
   */
  setEnabledButton() {
    if (this.state.name.value !== ''
      && this.state.name.error !== true
      && this.state.period.id !== '') {
      return false;
    } else {
      return true;
    }
  };
  
  /**
   * Render filled table
   * @return {*} - return component
   */
  render() {
    return (
      <Dialog
        open={this.props.open}
        onClose={this.props.onClose}
        onSubmit={this.props.onSubmit}
      >
        <DialogTitle>
          {this.props.action + " COURSE"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            <h3>Name:</h3>
            <TextField
              helperText="Input name of course"
              onChange={this.handleChangeName.bind(this)}
              value={this.state.name.value}
              error={this.state.name.error}
              label={this.state.name.label} />
            <br />
            <h3>Learn Period:</h3>
            <FormControl>
              <Select id='Periods'
                value={this.state.period.id}
                // name={}
                onChange={this.handleChangePeriod.bind(this)} >
                  {this.handleCut()}
              </Select>
              <FormHelperText>Input learn period</FormHelperText>
            </FormControl> 
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={this.props.onClose}
            color="primary" >
            Cancel
          </Button>
          <Button
            onClick={this.handleSubmit.bind(this)}
            color="secondary"
            disabled={this.setEnabledButton()}
            autoFocus >
            {this.props.action}
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}
  
export default withRouter(CoursesModal);
  