import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import CoursesList from "../Pages/Courses";
import { TeachersPage } from "../Pages/Teachers";
import { AddTeachersPage } from "../Pages/Teachers/addTeacher";
import { StudentsPage } from "../Pages/Students/index";
import { Student } from "../Pages/Students/Student"

const Router = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={StudentsPage} />
      <Route exact path="/teachers/" component={TeachersPage} />
      <Route exact path="/students/" component={StudentsPage} />
      <Route exact path="/students/new" component={Student} />
      <Route exact path="/students/:id" component={Student} />
      <Route exact path="/teachers/:id" component={AddTeachersPage} />
      <Route exact path='/courses/' component={CoursesList} />
    </Switch>
  </BrowserRouter>
);

export default Router;
